package com.lato;

public class TicTacToe {

    private int size;
    private char board[][];
    private char player;

    public boolean setSize(int size) {
        boolean check = validateSize(size);
        if (check)
            this.size = size;
        else
            System.out.println("Wrong size. Size has to be between 3 and 5");

        return check;
    }
    public void setBoard() {
        this.board = initializeBoard();
    }

    public boolean setPlayer(char player) {
        boolean check = validatePlayer(player);
        if (check)
            this.player = player;
         else
            System.out.println("Wrong player. Players has to be X or O");

        return check;
    }

    public int getSize() {
        return size;
    }

    public char[][] getBoard() {
        return board;
    }

    public char getPlayer() {
        return player;
    }

    public void showBoard() {
        for (int i = 0; i < board.length; i++)
            System.out.print("\t" + i);

        System.out.println();
        for (int row = 0; row < board.length; row++) {
            System.out.print(row + "\t");
            for (int column = 0; column < board.length; column++)
                System.out.print(board[row][column] + " \t");

            System.out.println();
        }
    }
    public char[][] initializeBoard() {
        char[][] board = new char[this.size][this.size];
        for (int row = 0; row < board.length; row++)
            for (int col = 0; col < board.length; col++)
                board[row][col] = '*';

        return board;
    }
    public boolean validateSize(int size) {
        if (size >= 3 && size <= 5) return true;
        else return false;
    }
    private boolean validatePlayer(char player) {
        if (player == 'O' || player == 'X') return true;
        else return false;
    }
    public void changePlayer() {
        setPlayer(getPlayer() == 'X' ? 'O' : 'X');
        System.out.println("\nNow " + getPlayer() + " moves.\n");
    }
    public void rules() {
        System.out.println("In the individual fields marked by this figure (if they were to be entered in a square, a single field would be a square with an edge of 1/3 of the described square), players place a circle and a cross alternately.");
        System.out.println("When one of the players creates a line " + getSize() + " crosses or circles, cross it out and become the winner. When all squares are full and no line is formed, the game ends in a draw.");
    }
    public boolean putCoordinatesOnBoard(int x, int y) {
        boolean isValid = false;
        if ((x >= 0 && x <= size - 1) && (y >= 0 && y <= size - 1)) {
            if (board[x][y] == '*') {
                board[x][y] = getPlayer();
                isValid = true;
            } else {
                System.out.println("Place is taken.");
                isValid = false;
            }
        }
        return isValid;
    }
    public boolean checkWinByRows(char player) {
        for (int column = 0; column < board.length; column++) {
            boolean win = true;
            for (int row = 0; row < board.length; row++) {
                if (board[column][row] != player) {
                    win = false;
                    break;
                }
            }
            if (win) return true;
        }
        return false;
    }
    public boolean checkWinByColumns(char player) {
        for (int column = 0; column < board.length; column++) {
            boolean win = true;
            for (int row = 0; row < board.length; row++) {
                if (board[row][column] != player) {
                    win = false;
                    break;
                }
            }
            if (win) return true;
        }
        return false;
    }
    public boolean checkWinByLeftToRightCross(char player) {
        int counter=0;
        for (int i = 0; i < board.length; i++) {
            if (board[i][i] == player)
                counter++;
            else
                counter=0;
        }
        return counter == board.length ? true : false;
    }
    public boolean checkWinByRightToLeftCross(char player) {
        int counter = 0;
        for (int i = board.length-1, j=0; i >= 0; i--, j++) {
            if (board[i][j] == player)
                counter++;
            else
                counter=0;
        }
        return counter == board.length ? true : false;
    }

    public boolean checkIfSomeoneWon(TicTacToe ttt) {
        if((ttt.checkWinByRows(ttt.getPlayer())) ||
                (ttt.checkWinByColumns(ttt.getPlayer())) ||
                (ttt.checkWinByLeftToRightCross(ttt.getPlayer()) ||
                        (ttt.checkWinByRightToLeftCross(ttt.getPlayer()))))
            return true;
        else
            return false;
    }
}