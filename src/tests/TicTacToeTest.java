package tests;

import com.lato.TicTacToe;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TicTacToeTest {

    private final char[][] byRows = {{0,0}, {1,1}, {0, 1}, {2, 0}, {0, 2}};
    private final int sizeT = 3;
    private final int sizeF = 7;

    TicTacToe o = new TicTacToe();

    @Test
    void validateSize_sizeValid_True() {
        assertTrue(o.validateSize(sizeT));
    }
    @Test
    void validateSize_sizeOver_False() {
        assertFalse(o.validateSize(sizeF));
    }

    @Test
    void checkWinByLeftToRightCross_XWinsOnSize3_ReturnTrue() {
        o.setSize(3);
        o.setBoard();
        char[][] boardpom = o.getBoard();
        boardpom[0][0] = 'X';
        boardpom[0][1] = 'O';
        boardpom[1][1] = 'X';
        boardpom[0][2] = 'O';
        boardpom[2][2] = 'X';
        assertTrue(o.checkWinByLeftToRightCross('X'));
    }

    @Test
    void checkWinByLeftToRightCross_XWinsOnSize4_ReturnTrue() {
        o.setSize(4);
        o.setBoard();
        char[][] boardpom = o.getBoard();
        boardpom[0][0] = 'X';
        boardpom[1][1] = 'X';
        boardpom[2][2] = 'X';
        boardpom[3][3] = 'X';
        assertTrue(o.checkWinByLeftToRightCross('X'));
    }

    @Test
    void checkWinByLeftToRightCross_XWinsOnSize5_ReturnTrue() {
        o.setSize(5);
        o.setBoard();
        char[][] boardpom = o.getBoard();
        boardpom[0][0] = 'X';
        //boardpom[0][1] = 'O';
        boardpom[1][1] = 'X';
        //boardpom[0][2] = 'O';
        boardpom[2][2] = 'X';
        boardpom[3][3] = 'X';
        boardpom[4][4] = 'X';
        assertTrue(o.checkWinByLeftToRightCross('X'));
    }

    @Test
    void checkWinByRows_XWinsOnSize5_ReturnTrue() {
        o.setSize(5);
        o.setBoard();
        char[][] boardpom = o.getBoard();
        boardpom[0][0] = 'X';
        boardpom[0][1] = 'X';
        boardpom[0][2] = 'X';
        boardpom[0][3] = 'X';
        boardpom[0][4] = 'X';
        assertTrue(o.checkWinByRows('X'));
    }

    @Test
    void checkWinByRightToLeftCross_XWinsOnSize4() {
        o.setSize(4);
        o.setBoard();
        char[][] boardpom = o.getBoard();

        boardpom[0][3] = 'O';
        boardpom[1][2] = 'O';
        boardpom[2][1] = 'O';
        boardpom[3][0] = 'O';


        assertTrue(o.checkWinByRightToLeftCross('O'));
    }

    @Test
    void checkWinByColumns_OWinsOnSize3_ReturnTrue() {
        o.setSize(3);
        o.setBoard();
        char[][] boardpom = o.getBoard();
        boardpom[0][0] = 'O';
        boardpom[1][0] = 'O';
        boardpom[2][0] = 'O';
        assertTrue(o.checkWinByColumns('O'));
    }
}